﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;
using Zelf.DAL.Context.Abstractions;
using Zelf.DAL.Entities.Abstractions;

namespace Zelf.DAL.Context.Implementations
{
    public class SqlLiteDBContext : DbContext, IDBContext
    {
        string DbPath { get; set; }

        public SqlLiteDBContext()
            :base()
        {}

        public new DbSet<TEntity> Set<TEntity>()
            where TEntity : BaseEntity
        {
            return base.Set<TEntity>();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await base.SaveChangesAsync(CancellationToken.None);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlite($"Data Source=zelf_sample.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<CustomerEntity>()
                .HasKey(f => f.ID);

            modelBuilder
                .Entity<CustomerEntity>()
                .Property(f => f.Name).IsRequired();

            modelBuilder
                .Entity<CustomerEntity>()
                .HasIndex(e => e.Name).IsUnique();

            modelBuilder
                .Entity<CustomerEntity>()
                .HasIndex(e => e.Name).IsUnique();


            modelBuilder
                .Entity<CustomerToCustomerRelationEntity>()
                .HasKey(f => new { f.SourceCustomerId, f.RelatedCustomerId });

            modelBuilder
                .Entity<CustomerToCustomerRelationEntity>()
                .HasOne(relation => relation.RelatedCustomer)
                .WithMany()
                .HasForeignKey(relation => relation.RelatedCustomerId);

            modelBuilder
                .Entity<CustomerToCustomerRelationEntity>()
                .HasOne(relation => relation.SourceCustomer)
                .WithMany(customer => customer.Subscriptions)
                .HasForeignKey(relation => relation.SourceCustomerId);
        }
    }
}
