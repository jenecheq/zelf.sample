﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Zelf.DAL.Entities.Abstractions;

namespace Zelf.DAL.Context.Abstractions
{
    public interface IDBContext
    {
        DbSet<TEntity> Set<TEntity>()
            where TEntity : BaseEntity;

        Task<int> SaveChangesAsync();
    }
}
