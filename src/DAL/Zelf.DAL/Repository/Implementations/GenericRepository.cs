﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Zelf.DAL.Context.Abstractions;
using Zelf.DAL.Entities.Abstractions;
using Zelf.DAL.Repository.Abstractions;

namespace Zelf.DAL.Repository.Implementations
{
    public class GenericRepository<TEntity> : IRepository<TEntity>
        where TEntity : BaseEntity
    {
        private readonly IDBContext _context;

        private DbSet<TEntity> _dbSet = null;

        public GenericRepository(IDBContext DBcontext)
        {
            _context = DBcontext;
        }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            var set = Set();

            var createdEntity = await set.AddAsync(entity);

            await _context.SaveChangesAsync();

            return createdEntity.Entity;
        }

        public IQueryable<TEntity> Get()
        {
            return Set();
        }

        private DbSet<TEntity> Set()
        {
            if (_dbSet == null)
            {
                _dbSet = _context.Set<TEntity>();
            }

            return _dbSet;
        }
    }
}
