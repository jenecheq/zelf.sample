﻿using System.Linq;
using System.Threading.Tasks;
using Zelf.DAL.Entities.Abstractions;

namespace Zelf.DAL.Repository.Abstractions
{
    public interface IRepository<TEntity>
        where TEntity : BaseEntity
    {
        Task<TEntity> AddAsync(TEntity entity);

        IQueryable<TEntity> Get();
    }
}
