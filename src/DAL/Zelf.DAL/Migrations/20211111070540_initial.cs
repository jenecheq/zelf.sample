﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zelf.DAL.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CustomerEntity",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerEntity", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "CustomerToCustomerRelationEntity",
                columns: table => new
                {
                    SourceCustomerId = table.Column<int>(nullable: false),
                    RelatedCustomerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerToCustomerRelationEntity", x => new { x.SourceCustomerId, x.RelatedCustomerId });
                    table.ForeignKey(
                        name: "FK_CustomerToCustomerRelationEntity_CustomerEntity_RelatedCustomerId",
                        column: x => x.RelatedCustomerId,
                        principalTable: "CustomerEntity",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerToCustomerRelationEntity_CustomerEntity_SourceCustomerId",
                        column: x => x.SourceCustomerId,
                        principalTable: "CustomerEntity",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomerEntity_Name",
                table: "CustomerEntity",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CustomerToCustomerRelationEntity_RelatedCustomerId",
                table: "CustomerToCustomerRelationEntity",
                column: "RelatedCustomerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomerToCustomerRelationEntity");

            migrationBuilder.DropTable(
                name: "CustomerEntity");
        }
    }
}
