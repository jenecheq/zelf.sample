﻿using System.Collections.Generic;

namespace Zelf.DAL.Entities.Abstractions
{
    public class CustomerEntity: BaseEntity
    {
        public string Name { get; set; }

        public IEnumerable<CustomerToCustomerRelationEntity> Subscriptions { get; set; }
    }
}
