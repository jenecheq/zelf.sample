﻿namespace Zelf.DAL.Entities.Abstractions
{
    public class CustomerToCustomerRelationEntity: ManyToManyRelation
    {
        public int SourceCustomerId { get; set; }

        public CustomerEntity SourceCustomer { get; set; }

        public int RelatedCustomerId { get; set; }

        public CustomerEntity RelatedCustomer { get; set; }
    }
}
