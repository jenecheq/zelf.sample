﻿using System.ComponentModel.DataAnnotations;

namespace Zelf.DAL.Entities.Abstractions
{
    public abstract class BaseEntity
    {
        [Key]
        public virtual int ID { get; set; }
    }
}
