﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Zelf.DAL.Entities.Abstractions
{
    public abstract class ManyToManyRelation: BaseEntity
    {
        [NotMapped]
        public override int ID { get; set; }
    }
}
