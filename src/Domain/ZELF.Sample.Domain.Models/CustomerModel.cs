﻿using System.Collections.Generic;

namespace ZELF.Sample.Domain.Models
{
    public class CustomerModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<CustomerModel> Relations { get; set; }
    }
}
