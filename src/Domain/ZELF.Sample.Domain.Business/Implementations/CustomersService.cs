﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Zelf.DAL.Entities.Abstractions;
using Zelf.DAL.Repository.Abstractions;
using ZELF.Sample.Domain.Business.Abstractions;
using ZELF.Sample.Domain.Models;

namespace ZELF.Sample.Domain.Business.Implementations
{
    public class CustomersService : ICustomersService
    {
        private readonly IRepository<CustomerEntity> _customersRepo;

        private readonly IRepository<CustomerToCustomerRelationEntity> _customerToCustomerRelationRepo;

        public CustomersService(
            IRepository<CustomerEntity> customersRepo,
            IRepository<CustomerToCustomerRelationEntity> customerToCustomerRelationRepo)
        {
            _customersRepo = customersRepo;

            _customerToCustomerRelationRepo = customerToCustomerRelationRepo;
        }

        public bool IsCustomerNameValid(string customerName)
        {
            var customerNamePattern = "^( ?)(([A-z ]+[ A-z]*?)( ?)([A-z ]*?)){1,64}$";

            var customerNameCpaceMandatoryPattern = "( +)";

            //only name with one ore more latin chars and one ore more spaces allowed

            return (
                Regex.IsMatch(customerName, customerNamePattern) &&
                Regex.IsMatch(customerName, customerNameCpaceMandatoryPattern)
            );
        }

        public async Task<CustomerModel> RegisterCustomerAsync(string customerName)
        {
            if (!IsCustomerNameValid(customerName))
            {
                return null;
            }

            //TODO:
            //Avoid using of Monitor / lock:
            //CustomerName field has unique constraint; exception (unique violation exc.) could be raised. wrap

            try
            {
                var createdEntity = await _customersRepo.AddAsync(new CustomerEntity { Name = customerName });

                //TODO
                //automapper or similar mechanism should go below
                return new CustomerModel
                {
                    Id = createdEntity.ID,
                    Name = createdEntity.Name,
                };
            }
            catch(Exception e)
            {
                //TODO
                //Send exception to elastic or somewhere else

                return null;
            }
        }

        public async Task<CustomerModel> SubscribeCustomerAsync(int currentCustomerId, int relatedCustomerId)
        {
            try
            {
                if (!await _customerToCustomerRelationRepo.Get().AnyAsync(r => r.RelatedCustomerId == relatedCustomerId && r.SourceCustomerId == currentCustomerId))
                {
                    await _customerToCustomerRelationRepo.AddAsync(new CustomerToCustomerRelationEntity { SourceCustomerId = currentCustomerId, RelatedCustomerId = relatedCustomerId });

                    return await GetCustomerById(currentCustomerId);
                }

                return null;
            }
            catch (Exception e)
            {
                //TODO
                //Send exception to elastic or somewhere else

                return null;
            }
        }

        public async Task<IEnumerable<CustomerModel>> GetTopRelatedCustomersAsync(int topCustomersCount)
        {
            var topCustomerEntities = await _customerToCustomerRelationRepo
                .Get()
                .GroupBy(r => r.RelatedCustomerId)
                .Select(group => new { RelatedCustomerId = group.Key, Count = group.Count() })
                .OrderBy(o => o.Count)
                .Take(topCustomersCount)
                .Select(c => c.RelatedCustomerId)
                .Join(_customersRepo.Get(), 
                    groupedTopCustomer => groupedTopCustomer,
                    customer => customer.ID,
                    (grouped, customer) => new CustomerEntity { ID = customer.ID, Name = customer.Name }
                )
                .ToArrayAsync();

            return topCustomerEntities.Select(topCustomer => new CustomerModel { Id = topCustomer.ID });
        }

        public async Task<IEnumerable<CustomerModel>> GetCustomersAsync()
        {
            //TODO
            //automapper or similar mechanism should go below
            var customers = await QueryCustomers().ToArrayAsync();

            return customers;
        }

        private async Task<CustomerModel> GetCustomerById(int customerId)
        {
            //TODO
            //automapper or similar mechanism should go below
            var foundCustomer = await QueryCustomers()
                                        .SingleOrDefaultAsync(c => c.Id == customerId);

            return foundCustomer;
        }

        private IQueryable<CustomerModel> QueryCustomers()
        {
            //TODO
            //automapper or similar mechanism should go below
            var customersQuery = _customersRepo
                .Get()
                .Include(include => include.Subscriptions)
                .ThenInclude(include => include.RelatedCustomer)                
                .Select(customer => new CustomerModel
                {
                    Id = customer.ID,
                    Name = customer.Name,
                    Relations = customer.Subscriptions.Select(relatedCustomer => new CustomerModel
                    {
                        Id = relatedCustomer.RelatedCustomer.ID,
                        Name = relatedCustomer.RelatedCustomer.Name
                    })
                });

            return customersQuery;
        }
    }
}
