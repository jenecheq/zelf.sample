﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ZELF.Sample.Domain.Models;

namespace ZELF.Sample.Domain.Business.Abstractions
{
    public interface ICustomersService
    {
        bool IsCustomerNameValid(string customerName);

        Task<CustomerModel> RegisterCustomerAsync(string customerName);

        Task<IEnumerable<CustomerModel>> GetCustomersAsync();

        Task<CustomerModel> SubscribeCustomerAsync(int currentCustomerId, int subscriberCustomerId);

        Task<IEnumerable<CustomerModel>> GetTopRelatedCustomersAsync(int topCustomersCount);
    }
}
