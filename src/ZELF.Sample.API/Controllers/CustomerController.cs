﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using ZELF.Sample.Domain.Business.Abstractions;
using ZELF.Sample.Domain.Models;

namespace ZELF.Sample.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        private readonly ICustomersService _customersService;

        public CustomerController(ICustomersService customersService)
        {
            _customersService = customersService;
        }

        /// <summary>
        /// Register a new customer with unique name
        /// </summary>
        /// <param name="customerInitials">unique name; must contain only digits and letters</param>
        /// <returns></returns>
        [HttpPost("{customerInitials}")]
        [ProducesResponseType(typeof(CustomerModel), StatusCodes.Status200OK)]
        [ProducesResponseType(400)]
        [ProducesResponseType(409)]
        public async Task<IActionResult> Register(string customerInitials)
        {
            //TODO:
            //KISS: I wont use a model validation mechanism for a contract with one field. As I wont create a contract as well )

            if (!_customersService.IsCustomerNameValid(customerInitials))
            {
                return BadRequest();
            }

            var createdCustomer = await _customersService.RegisterCustomerAsync(customerInitials);

            if (createdCustomer == null)
            {
                return Conflict();
            }

            return Ok(createdCustomer);
        }

        /// <summary>
        /// Returns list of all existing customers
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<CustomerModel>), StatusCodes.Status200OK)]        
        public async Task<IActionResult> ListCustomers()
        {
            var existingCustomers = await _customersService.GetCustomersAsync();

            return Ok(existingCustomers);
        }

        /// <summary>
        /// Subscribe <paramref name="currentCustomerId"/> current customer on anyone else
        /// </summary>
        /// <param name="currentCustomerId"></param>
        /// <param name="relatedCustomerId"></param>
        /// <returns></returns>
        [HttpPatch("{currentCustomerId}/on/{relatedCustomerId}")]
        [ProducesResponseType(typeof(CustomerModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<IActionResult> Subscribe(int currentCustomerId, int relatedCustomerId)
        {
            var updatedCurrentCustomer = await _customersService.SubscribeCustomerAsync(currentCustomerId, relatedCustomerId);

            if (currentCustomerId == relatedCustomerId)
            { 
                return BadRequest("what a snob (facepalm)");
            }

            if (updatedCurrentCustomer == null)
            {
                return Conflict();
            }

            return Ok(updatedCurrentCustomer);
        }

        /// <summary>
        /// Returns top customers with highest amount of subscribers
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        [HttpGet("{count}")]
        [ProducesResponseType(typeof(IEnumerable<CustomerModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> TopRelatedCustomers(int count = 10)
        {
            var topRelatedCustomers = await _customersService.GetTopRelatedCustomersAsync(count);            

            return Ok(topRelatedCustomers);
        }
    }
}
