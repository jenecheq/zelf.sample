using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Zelf.DAL.Context.Abstractions;
using Zelf.DAL.Context.Implementations;
using Zelf.DAL.Repository.Abstractions;
using Zelf.DAL.Repository.Implementations;
using ZELF.Sample.Domain.Business.Abstractions;
using ZELF.Sample.Domain.Business.Implementations;

namespace ZELF.Sample.API
{
    public class Startup
    {
        readonly IHostEnvironment _env;

        public Startup(IHostEnvironment env)
        {
            _env = env;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddDbContext<IDBContext, SqlLiteDBContext>()
                .AddScoped(typeof(IRepository<>), typeof(GenericRepository<>));

            services
                .AddScoped<ICustomersService, CustomersService>();

            if (_env.IsDevelopment())
            {
                services.AddSwaggerGen(c =>
                {
                    c.SwaggerDoc("v1", new OpenApiInfo { Title = "ZELF.Sample API", Version = "v1" });

                    try
                    {
                        var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                        var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                        c.IncludeXmlComments(xmlPath, includeControllerXmlComments: true);
                    }
                    catch
                    { }
                });
            }

            services
                .AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (_env.IsDevelopment())
            {
                app
                    .UseSwagger()
                    .UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "ZELF.Sample API doc"));
            }

            app
                .UseRouting()
                .UseEndpoints(endpoints =>
                {
                    endpoints.MapControllerRoute(
                        name: "default",
                        pattern: "api/{controller}/{action}");
                });
        }
    }
}
