﻿namespace ZELF.Sample.Test.IntegrationTests.Mock
{
    public class CustomerMock
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
