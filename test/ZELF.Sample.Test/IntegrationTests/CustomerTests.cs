﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using ZELF.Sample.API;
using ZELF.Sample.Test.IntegrationTests.Mock;

namespace ZELF.Sample.Test.IntegrationTests
{
    public class CustomerTests : IClassFixture<TestFixture<Startup>>
    {
        private HttpClient client;

        public CustomerTests(TestFixture<Startup> fixture)
        {
            client = fixture.client;
        }

        [Fact]
        public async Task TestGetCustomersEndpointAsync()
        {
            // Arrange
            var request = "/api/customer";

            // Act
            var response = await client.GetAsync(request);

            // Assert            
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task TestRegisterCustomerEndpointAsync()
        {
            // Arrange
            var customerName = "john doe";
            var request = $"/api/customer/{customerName}";

            // Act
            var response = await client.PostAsync(request, new StringContent(customerName));

            // Assert            
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task TestRegisterCustomerBadNameAsync()
        {
            // Arrange
            /// name must contain letters and spaces
            var customerName = "mamkin haxx0r";
            var request = $"/api/customer/{customerName}";

            // Act
            var response = await client.PostAsync(request, null);

            // Assert            
            Assert.Equal(StatusCodes.Status400BadRequest, (int)response.StatusCode);
        }

        /// <summary>
        /// asdasd
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task TestRegisterCustomerUniqueNameAsync()
        {
            // Arrange
            var customerName = "Nomad korovan";
            var request = $"/api/customer/{customerName}";

            // Act
            var firstRegistrationResponse = await client.PostAsync(request, null);
            var secondRegistrationResponse = await client.PostAsync(request, null);

            // Assert            
            Assert.Equal(StatusCodes.Status409Conflict, (int)secondRegistrationResponse.StatusCode);
        }

        [Fact]
        public async Task TestCustomerSubscriptionAsync()
        {
            // Arrange
            var firstCustomerName = "aliexpress rasprodazha";
            var firstRegistrationRequest = $"/api/customer/{firstCustomerName}";

            var secondCustomerName = "ozon net sobstennih idey";
            var secondRegistrationRequest = $"/api/customer/{secondCustomerName}";

            var subscribeRequest = "/api/customer";

            // Act
            var aliexpressRegistrationResponse = await client.PostAsync(firstRegistrationRequest, null);
            var ozonRegistrationResponse = await client.PostAsync(secondRegistrationRequest, null);

            var aliexpressMock = JsonConvert.DeserializeObject<CustomerMock>(await aliexpressRegistrationResponse.Content.ReadAsStringAsync());
            var ozonMock = JsonConvert.DeserializeObject<CustomerMock>(await ozonRegistrationResponse.Content.ReadAsStringAsync());

                ///subscribe OZON to Aliexpress as OZON is the current user wants to get noticed of aliexpress feed
            var subscriptionResponse = await client.PatchAsync($"{subscribeRequest}/{ozonMock.Id}/on/{aliexpressMock.Id}", null);

            // Assert            
            subscriptionResponse.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task TestTopCustomersListEndpointAsync()
        {
            var topSubscribersRequest = $"/api/customer/{Int32.MaxValue}";

            // Act
            var topCustomersResponse = await client.GetAsync(topSubscribersRequest);

            // Assert            
            topCustomersResponse.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task TestTopCustomersListAsync()
        {
            // Arrange
            var firstCustomerName = "patrick star";
            var firstRegistrationRequest = $"/api/customer/{firstCustomerName}";

            var secondCustomerName = "sponge bob";
            var secondRegistrationRequest = $"/api/customer/{secondCustomerName}";

            var subscribeRequest = "/api/customer";

            var topSubscribersRequest = $"/api/customer/{Int32.MaxValue}";

            // Act
            var patrickRegistrationResponse = await client.PostAsync(firstRegistrationRequest, null);
            var spongeRegistrationResponse = await client.PostAsync(secondRegistrationRequest, null);

            var patrickMock = JsonConvert.DeserializeObject<CustomerMock>(await patrickRegistrationResponse.Content.ReadAsStringAsync());
            var spongeMock = JsonConvert.DeserializeObject<CustomerMock>(await spongeRegistrationResponse.Content.ReadAsStringAsync());

                ///subscribe Patrick to Bob
            var subscriptionResponse = await client.PatchAsync($"{subscribeRequest}/{patrickMock.Id}/on/{spongeMock.Id}", null);
            
            var topCustomersResponse = await client.GetAsync(topSubscribersRequest);

            var topCustomersMock = JsonConvert.DeserializeObject<IEnumerable<CustomerMock>>(await topCustomersResponse.Content.ReadAsStringAsync());

            // Assert            
            Assert.Contains(topCustomersMock, customer => customer.Id == spongeMock.Id);
        }
    }
}
