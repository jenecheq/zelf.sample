﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using Zelf.DAL.Context.Implementations;

namespace ZELF.Sample.Test.IntegrationTests
{
    public class TestFixture<TStartup>: IDisposable
    {
        const string zelfDatabaseName = "zelf_sample.db";

        public HttpClient client { get; private set; }

        private TestServer server;

        public TestFixture()
        {
            var testServerUrl = "http://localhost:5001";

            var webHostBuilder = new WebHostBuilder()                
                .UseUrls(testServerUrl)
                .UseEnvironment("Development")
                .UseStartup(typeof(TStartup));

            //TODO:
            //the right approach is to set the realization of IDBContext to SQLInMemory
            //but sample project needs some trash: recreate SQLite database each time )
            if (File.Exists(zelfDatabaseName))
            {
                File.Delete(zelfDatabaseName);                
            }

            try
            {
                new SqlLiteDBContext().Database.Migrate();
            }
            catch(Exception)
            {}

            server = new TestServer(webHostBuilder);            

            client = server.CreateClient();
            client.BaseAddress = new Uri(testServerUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        public void Dispose()
        {
            if (File.Exists(zelfDatabaseName))
            {
                File.Delete(zelfDatabaseName);
            }
            client.Dispose();
            server.Dispose();
        }
    }
}
